<?php

/*
 * Deploy web pages
 *
 * */

// ################## INCLUDE #####################
$path_Util		= "../scripts/utils/Util.php";
include ($path_Util);

// ############### UTILS & DEBUG ##################
$util = new Util();
$util->setDebug("error");

// ################## CONSTANTS #####################
$nbOfArguments		= 1;
$nbOfArgumentsDebug	= $nbOfArguments + 1;
$error			= "### ERROR ###";
$man			= "You should call script as follows :" . PHP_EOL .
			  "> php deployWeb.php (debug)";
$scriptEnds 		= "### SCRIPT ENDS ###" ;

$pathToWebFolder	= "../../../../web/";
$url			= "framastats.org/";

$webFolder_local	= "../web/";

// ################### SCRIPT #####################

// Check number of arguments
if ($argc < $nbOfArguments || $argc > $nbOfArgumentsDebug) {
	$util->out($error . PHP_EOL . $man . PHP_EOL . $scriptEnds, "error", true); // exit
}
// Debug Mode
elseif ($argc == $nbOfArgumentsDebug) {
	$mode = $argv[$nbOfArgumentsDebug -1];
	if ($mode == "debug") {
		$util->setDebug("all");
		$modeUpperCase =  mb_strtoupper($mode);
		$util->out("### $modeUpperCase MODE", "info");
	} else {
		$util->out($error . PHP_EOL . $man . PHP_EOL . $scriptEnds, "error", true); // exit
	}
}

// Confirm the path
if ($util->confirmationToWriteInThisFolder($pathToWebFolder) == 'yes')
{
	$util->out("### Try to copy file in this path : " . $pathToWebFolder, 'info');

	// Copy the web folder local to web folder
	$resCopy = ($util->recurse_copy($webFolder_local, $pathToWebFolder)) ? "was a success. Public adress should be : $url" : "failed !!" ;
}
else
{
	$resCopy = "failed !!" ;
}
$color = ($resCopy == "failed !!" ) ? "error" : "success";
$util->out("### Online deployment " . $resCopy, $color);
