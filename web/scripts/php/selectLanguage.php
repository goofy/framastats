<?php

// ################### MAIN ####################

// Find language among availablesLanguages
$availablesLanguages = array('fr');
$lang = autoSelectLanguage($availablesLanguages, 'fr');
//$lang = 'fr';

// Include right lang file
$pathToLangFile = 'lang/' . $lang . '-lang.php';
include($pathToLangFile);


// ################# FUNCTIONS #################

/**
 * Automatic detection of browser language
 *
 * Language code should be necesseraly in 2 chars
 *
 * @param array 1D $aLanguages availables langs
 * @param string $sDefault default lang if not found
 * @return string browser lang or default
 * @author Hugo Hamon
 * @version 0.1
 * @url http://huit.re/autoSelectLanguage
 */
function autoSelectLanguage($aLanguages, $sDefault = 'fr') {
	if(!empty($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
		$aBrowserLanguages = explode(',',$_SERVER['HTTP_ACCEPT_LANGUAGE']);
			foreach($aBrowserLanguages as $sBrowserLanguage) {
				$sLang = strtolower(substr($sBrowserLanguage,0,2));
				if(in_array($sLang, $aLanguages)) {
					return $sLang;
				}
			}
	}
	return $sDefault;
}
?>
