				<!-- Pattern Framaxxx -->
				<div id="Framaxxx" class="divsCenter">
					<div class="row serviceNameRow">
						<div class="col-md-8 col-xs-12">
							<h2>
							<span class="glyphicon glyphicon-tree-deciduous" aria-hidden="true"></span> <!-- Change here icon with glyphicon or fontawesome icon -->
							Framaxxx
							</h2>
						</div>
						<!-- Update time large devices -->
						<div class="col-md-4 visible-md-inline visible-lg-inline timeUpdate_large"> 
							<h6 class="textUpdate_large"><?echo TXT_UPDATE;?><span class="statToFill framaxxx_lastUpdate"></span> </h6> <!-- Change name of service -->
						</div>
						<!-- Update time small devices -->
						<div class="visible-xs-block visible-sm-block col-xs-12 timeUpdate_small"> 
							<h6><?echo TXT_UPDATE;?><span class="statToFill framaxxx_lastUpdate"></span></h6> <!-- Change name of service -->
						</div>
					</div>
					<h6><?php echo TXT_FRAMAXXX_DESCRIPTION;?> <a href="http://framaxxx.org/" target="_blank"> <?php echo TXT_FRAMAXXX_DESCRIPTION_LIEN; ?>.</a> </h6> <!-- Change link href and constant -->
					<div class="centerStats">
						<div class="panel-group">
							<!-- Pattern : all kind of panels -->
							<!-- 1st Stat Framaxxx --> <!-- Title with stat + panel collapsing = Framasoft 1st stat -->
							<div class="panel panel-default clickable">
								<!-- Panel heading clickable-->
								<a data-toggle="collapse" href="#collapseOne_framaxxx" aria-haspopup="true" aria-expanded="false"> <!-- Change href -->
									<div class="panel-heading">
										<h4 class="panel-title">
											<div class="row"> 
												<div class="col-md-3 col-xs-2">
													<div class="statToFill statInTitle framaxxx_stat1"><?php echo TXT_SHOULD_NOT_APPEAR ;?></div>  <!-- Change class with stat name but don't touch at "statToFill statInTitle"-->
												</div>
												<div class="col-md-8 col-xs-8">
													<div class="statNameInTitle">
														<?php echo TXT_FRAMAXXX_STATNAME ;?>  <!-- Change constant -->
													</div>
												</div>
												<div class="col-md-1 col-xs-1 icon">
													<span class="glyphicon glyphicon-chevron-down" aria-hidden="true" <?php echo TXT_LABEL_CHEVRON_DOWN ;?>></span>
												</div>
											</div>
										</h4>
									</div>
								</a>
								<!-- Panel collapse-->
								<div class="panel-collapse collapse withChevron" id="collapseOne_framaxxx" > <!-- Change id which should be the same as href -->
									<div class="panel-body">
										<p><?php echo TXT_FRAMAXXX_STATNAME2_1; ?><strong><span class="statToFill framaxxx_stat2"></span></strong><?php echo TXT_FRAMAXXX_STATNAME2_2 ;?><strong><span class="statToFill framaxxx_stat2"></span></strong><?php echo TXT_FRAMAXXX_STATNAME2_3 ;?></p> <!-- Change constant and class in span but don't touch at "statToFill"-->
									</div>
								</div>
							</div>
							<!-- 2nd Stat Framaxxx --> <!-- Title with stat = Framasoft 2nd stat -->
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="row">
											<div class="col-md-3 col-xs-2">
												<div class="statToFill statInTitle framaxxx_stat3"><?php echo TXT_SHOULD_NOT_APPEAR; ?></div> <!-- Change class with stat name but don't touch at "statToFill statInTitle"-->
											</div>
											<div class="col-md-8 col-xs-8">
												<div class="statNameInTitle"><?php echo TXT_FRAMAXXX_COUNT_SERVICES ;?></div> <!-- Change constant -->
											</div>
										</div>
									</h4>

								</div>
							</div>
							<!-- 3th Stat Framaxxx --> <!-- Title with stat + panel with list = Framasoft 7th stat -->
							<div class="panel panel-default clickable">
								<!-- Panel heading clickable-->
								<a data-toggle="collapse" href="#collapseTwo_framaxxx" aria-haspopup="true" aria-expanded="false"> <!-- Change href -->
									<div class="panel-heading">
										<h4 class="panel-title">
											<div class="row"> 
												<div class="col-md-3 col-xs-2">
													<div class="statToFill statInTitle framaxxx_stat4"><?php echo TXT_SHOULD_NOT_APPEAR ;?></div> <!-- Change class with stat name but don't touch at "statToFill statInTitle"-->
												</div>
												<div class="col-md-8 col-xs-8">
													<div class="statNameInTitle"><?php echo TXT_FRAMAXXX_VISIT_ALL_TITLE ;?></div>	<!-- Change constant -->												
												</div>
												<div class="col-md-1 col-xs-1 icon">
													<span class="glyphicon glyphicon-chevron-down" aria-hidden="true" <?php echo TXT_LABEL_CHEVRON_DOWN ;?>></span>
												</div>
											</div>
										</h4>
									</div>
								</a>
								<!-- Panel collapse-->
								<div class="panel-collapse collapse withChevron" id="collapseTwo_framaxxx" > <!-- Change id -->
									<div class="panel-body">
										<ul class="list-group"> 
										  <li class="list-group-item"> <span class="badge statToFill framaxxx_stat4">...</span> <?echo TXT_THIS_YEAR ;?> </li> <!-- Change class with stat name but don't touch at "badge statToFill "--> <!-- Change constant in echo "--> 
										  <li class="list-group-item"> <span class="badge statToFill framaxxx_stat5">...</span> <?echo TXT_1_LAST_YEAR ;?> </li> 
										  <li class="list-group-item"> <span class="badge statToFill framaxxx_stat6">...</span> <?echo TXT_6_LAST_MONTHS ;?> </li>
										</ul>
									</div>
								</div>
							</div>
							<!-- 4th Stat Framaxxx --> <!-- Title with stat + chart = Framasoft 9th stat -->
							<div class="panel panel-default clickable">
								<!-- Panel heading clickable-->
								<a data-toggle="collapse" href="#" id="framaxxx_nameChartStat_click" aria-haspopup="true" aria-expanded="false"> <!-- Change id : will be used in javascript-->
									<div class="panel-heading">
										<h4 class="panel-title">
											<div class="row">
												<div class="col-md-3 col-xs-2">
													<div class="statToFill statInTitle framaxxx_stat7"><?php echo TXT_SHOULD_NOT_APPEAR ;?></div> <!-- Change class with stat name but don't touch at "statToFill statInTitle"-->
												</div>
												<div class="col-md-8 col-xs-8">
													<div class="statNameInTitle"><?php echo TXT_FRAMAXXX_VISIT_TODAY_TITLE ;?></div> <!-- Change constant -->
												</div>
												<div class="col-md-1 col-xs-1 icon">
													<span class="glyphicon glyphicon-stats" aria-hidden="true" <?php echo TXT_LABEL_ICON_STAT ;?>></span>
												</div>
											</div>
										</h4>
									</div>
								</a>
								<div>
									<canvas class="notDisplayChart charts" id="framaxxx_nameChartStat" width="300" height="150"></canvas> <!-- Change id : will be used in javascript. Use the same as you right in calculateChartsStats.php-->
								</div>
							</div>
							<!-- 5th Stat Framaxxx --> <!-- Title with stat + panel + chart = Framadvd 1st stat -->
							<div class="panel panel-default clickable">
								<!-- Panel heading clickable-->
								<a data-toggle="collapse" href="#collapseOne_framaxxx" id="framaxxx_nameChartStat_click" aria-haspopup="true" aria-expanded="false"> <!-- Change id : will be used in javascript-->
									<div class="panel-heading">
										<h4 class="panel-title">
											<div class="row">
												<div class="col-md-3 col-xs-2">
													<div class="statToFill statInTitle framaxxx_stat7"><?php echo TXT_SHOULD_NOT_APPEAR ;?></div> <!-- Change class with stat name but don't touch at "statToFill statInTitle"-->
												</div>
												<div class="col-md-8 col-xs-8">
													<div class="statNameInTitle"><?php echo TXT_FRAMAXXX_VISIT_TODAY_TITLE ;?></div> <!-- Change constant -->
												</div>
												<div class="col-md-1 col-xs-1 icon">
													<span class="glyphicon glyphicon-stats" aria-hidden="true" <?php echo TXT_LABEL_ICON_STAT ;?>></span>
												</div>
											</div>
										</h4>
									</div>
								</a>
								<!-- Panel collapse-->
								<div class="panel-collapse collapse" id="collapseOne_framaxxx" >
									<div class="panel-body">
										<h6><?php echo TXT_FRAMAXXX_PANEL_TXT ;?></h6>
									</div>
								</div>
								<div>
									<canvas class="notDisplayChart charts" id="framaxxx_nameChartStat" width="300" height="150"></canvas> <!-- Change id : will be used in javascript. Use the same as you right in calculateChartsStats.php-->
								</div>
							</div>
						</div>
					</div>
				</div>
