<?php
// ============================================================
// Script called very often by cronjob :
// Calculate all stats needed in framastats.org
//
// Permits to be a lot more efficient when called framastats.org
// because we just retrieve the latest stats already calculated
// =============================================================

// Calculate Stats and put the results in html
$nameHTMLFile 		= "calculatedStats";
$nameJSONFile_Chart	= "calculatedChartsStats";

$cmd			= "(php calculateStats.php) > ../../admin/" . $nameHTMLFile . ".html";
shell_exec($cmd);

$cmd			= "(php calculateChartsStats.php) > ../../admin/" . $nameJSONFile_Chart . ".html";
shell_exec($cmd);

?>
