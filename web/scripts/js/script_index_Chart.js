//When DOM loaded
$(document).ready(function() {
	
	/*
	 * Handle the display of alls charts
	 * */
	function handleChart (chartId) {
		var selector = "#" + chartId;
		if ($(selector).hasClass("notDisplayChart")) {
			$(selector).removeClass("notDisplayChart");
			displayChart (chartId);
		} else {
			$(selector).addClass("notDisplayChart");
		}
	} 
	
	/*
	 * Called by handleChart
	 * Create chart according to their options
	 * */
	function displayChart (chartId) {

		// Retrieve charts stats
		var selectStats = ".chartsStats" + "." + chartId;
		var datasHTML 	= $(selectStats).html();
		var datasJSON 	= JSON.parse(datasHTML);

		var ctx = document.getElementById(chartId).getContext("2d");
		switch (chartId)
		{
			case 'framasoft_chartToday':
				var myNewChart = new Chart(ctx).Line(datasJSON, {
					responsive : true
					});
				break;
			case 'framasoft_chartAll':
				var myNewChart = new Chart(ctx).Line(datasJSON, {
					responsive : true
					});
				break;
			case 'framadate_chartToday':
				var myNewChart = new Chart(ctx).Line(datasJSON, {
					responsive : true
					});
				break;
			case 'framadate_chartAll':
				var myNewChart = new Chart(ctx).Line(datasJSON, {
					responsive : true
					});
				break;
			case 'framabook_chartToday':
				var myNewChart = new Chart(ctx).Line(datasJSON, {
					responsive : true
					});
				break;
			case 'framasphere_users':
				var myNewChart = new Chart(ctx).Line(datasJSON, {
					responsive : true,
					multiTooltipTemplate: "<%= datasetLabel %> - <%= value %>"
					});
				break;
			case 'framasphere_posts':
				var myNewChart = new Chart(ctx).Line(datasJSON, {
					responsive : true
					});
				break;
			case 'framasphere_comments':
				var myNewChart = new Chart(ctx).Line(datasJSON, {
					responsive : true
					});
				break;
			case 'framadvd_chartAll':
				var myNewChart = new Chart(ctx).Line(datasJSON, {
					responsive : true
					});
				break;
			case 'framabin_chartDocs':
				var myNewChart = new Chart(ctx).Line(datasJSON, {
					responsive : true,
					multiTooltipTemplate: "<%= datasetLabel %> - <%= value %>"
					});
				break;
			case 'twitter_followers':
				var myNewChart = new Chart(ctx).Line(datasJSON, {
					responsive : true
					});
				break;
			case 'twitter_tweets':
				var myNewChart = new Chart(ctx).Line(datasJSON, {
					responsive : true
					});
				break;
			case 'facebook_likes':
				var myNewChart = new Chart(ctx).Line(datasJSON, {
					responsive : true
					});
				break;
		}
	}
	
	$( "#framasoft_chartToday_click" ).click(function() {
		handleChart ('framasoft_chartToday');
	});
	$( "#framasoft_chartAll_click" ).click(function() {
		handleChart ('framasoft_chartAll');
	});
	$( "#framadate_chartToday_click" ).click(function() {
		handleChart ('framadate_chartToday');
	});
	$( "#framadate_chartAll_click" ).click(function() {
		handleChart ('framadate_chartAll');
	});
	$( "#framabook_chartToday_click" ).click(function() {
		handleChart ('framabook_chartToday');
	});
	$( "#framasphere_users_click" ).click(function() {
		handleChart ('framasphere_users');
	});
	$( "#framasphere_posts_click" ).click(function() {
		handleChart ('framasphere_posts');
	});
	$( "#framasphere_comments_click" ).click(function() {
		handleChart ('framasphere_comments');
	});
	$( "#framadvd_chartAll_click" ).click(function() {
		handleChart ('framadvd_chartAll');
	});
	$( "#framabin_chartDocs_click" ).click(function() {
		handleChart ('framabin_chartDocs');
	});
	$( "#twitter_followers_click" ).click(function() {
		handleChart ('twitter_followers');
	});
	$( "#twitter_tweets_click" ).click(function() {
		handleChart ('twitter_tweets');
	});
	$( "#facebook_likes_click" ).click(function() {
		handleChart ('facebook_likes');
	});

});


