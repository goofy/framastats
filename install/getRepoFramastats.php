<?php
	$repoName = "framastats/";
	// Need a sshKey for the machine in order to avoid logging
	if(!file_exists($repoName) || !is_dir($repoName)){
		// Need to clone the repo
		echo "Start cloning repo..." . PHP_EOL;
		shell_exec('git config --global user.name "Quentin Dupont"');
		shell_exec('git config --global user.email "quentin.dupont@framasoft.org"');
		shell_exec('git clone git@git.framasoft.org:quentin.dupont/framastats.git');
	} else {
		// Updating repo
		chdir($repoName);
		$reponseGit = shell_exec('git pull');
		echo "########## GIT #########" . PHP_EOL . getcwd() . " ---> " . $reponseGit . "######## FIN GIT #######" . PHP_EOL;
	}
?>
