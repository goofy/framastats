<?php

/*
 * include by logsApache/mainScript.php
 *
 * Create $stats
 *
 * */

// ################## CONSTANTS ###################
$totalDownloads_start		= 0;
$statusCode 			= 200;
$minBytesDownloaded_Ecole	= 4543205376;
$minBytesDownloaded_Classic 	= 4572856320;
$referersDvdEcole 		= array ("\"http://framadvd.org/framadvd-ecole\"");
$referersDvdClassic 		= array ("\"http://framadvd.org/framadvd\"");

$regex_dayOfLastLog 		= "/[0-9]{2}.\/[a-zA-Z]+.\/[0-9]{4}/";
$regex_framadvdEcole 		= "/^.*GET \/ecole\/ISO\/framadvd_ecole.*iso.*$/";
$regex_framadvdClassic 		= "/^.*GET \/standard\/ISO\/framaDVD2RC1.*iso.*$/";
$regex_control 			= "/.iso [A-Z]*\/([0-9]*).?([0-9]*)\" ([0-9]*) ([0-9]*) (\".*\") (\".*\")/";
				// capturing group 3 ==> status code HTTP
				// capturing group 4 ==> bytes Downloaded
				// capturing group 5 ==> referer
				// capturing group 6 ==> client browser

// ################## FUNCTIONS ###################

/*
 * Create the instance of Stats that will be deploy online
 * Called when it has never been created before
 * @_downloads_today_ecole : number of daily downloads
 * @_downloads_today_classic : number of daily downloads
 * @_logDate : permits to know if we already make the stats
 * return $stats
 * */
function createStats_init ($stats, $_downloads_today_ecole, $_downloads_today_classic, $_logDate)
{
	global $totalDownloads_start;

	$stats->logs['firstDayOfStats'] 	= date('Y-m-d');
	$stats->logs['downloads_today_ecole']	= $_downloads_today_ecole;
	$stats->logs['downloads_today_classic'] = $_downloads_today_classic;
	$stats->logs['downloads_all'] 		= $totalDownloads_start + $stats->logs['downloads_today_ecole'] + $stats->logs['downloads_today_classic'];
	$stats->logs['lastDayUpdateStats'] 	= $_logDate;
	$stats->logs['timeUpdateStats'] 	= date('Y-m-d H:i:s');
	return $stats;
}

/*
 * Create the instance of Stats that will be deploy online
 * Called when stats has already been initialized (see createStats_init)
 * @_downloads_today_all : number of downloads in all time
 * return $stats
 * */
function createStats ($stats, $_downloads_today_ecole, $_downloads_today_classic, $_downloads_today_all, $_logDate, $_firstDayOfStats)
{
	$stats->logs['firstDayOfStats'] 	= $_firstDayOfStats;
	$stats->logs['downloads_today_ecole'] 	= $_downloads_today_ecole;
	$stats->logs['downloads_today_classic'] = $_downloads_today_classic;
	$stats->logs['downloads_all'] 		= $_downloads_today_all;
	$stats->logs['lastDayUpdateStats'] 	= $_logDate;
	$stats->logs['timeUpdateStats'] 	= date('Y-m-d H:i:s');
	return $stats;
}


// ################### SCRIPT #####################

// Get all lines of downloading
$a_logsFramadvdEcole 	= preg_grep($regex_framadvdEcole, $a_logs);
$a_logsFramadvdClassic 	= preg_grep($regex_framadvdClassic, $a_logs);

// Count how many downloads we had before treatement
$numberOfDownloads_Ecole = count($a_logsFramadvdEcole);
$numberOfDownloads_Classic = count($a_logsFramadvdClassic);

// $util->out("AVANT_ECOLE : " . $util->array_toString($a_logsFramadvdEcole));
// $util->out("AVANT_CLASSIC : " . $util->array_toString($a_logsFramadvdClassic));

// Remove false positive
$a_logsFramadvdEcole = Util_logsApache::removeFalsePositive($a_logsFramadvdEcole, $regex_control, $statusCode, $minBytesDownloaded_Ecole, $referersDvdEcole);
$a_logsFramadvdClassic = Util_logsApache::removeFalsePositive($a_logsFramadvdClassic, $regex_control, $statusCode, $minBytesDownloaded_Classic, $referersDvdClassic);

// Count how many downloads we had
$finalNumberOfDownloads_Ecole = count($a_logsFramadvdEcole);
$finalNumberOfDownloads_Classic = count($a_logsFramadvdClassic);

// $util->out("APRES_ECOLE : " . $util->array_toString($a_logsFramadvdEcole));
// $util->out("APRES_CLASSIC : " . $util->array_toString($a_logsFramadvdClassic));

$resEcole = $numberOfDownloads_Ecole - $finalNumberOfDownloads_Ecole;
$resClassic = $numberOfDownloads_Classic - $finalNumberOfDownloads_Classic;
$util->out("### Remove $resEcole downloads for DvdEcole and $resClassic for DvdClassic", "info");
$util->out("### Final number of downloads for DvdEcole : $finalNumberOfDownloads_Ecole", "info");
$util->out("### Final number of downloads for DvdClassic : $finalNumberOfDownloads_Classic", "info");

// Retrieve (in the log) day of logs
preg_match($regex_dayOfLastLog, json_encode($a_logs), $matches);
$logDate = $matches[0];

// Temporary-file and saving-stats management
if (!file_exists($tmp_pathFileName))
{
	$util->out("### Temporary stats file has never been created. ", "info");
	$stats = createStats_init ($stats, $finalNumberOfDownloads_Ecole, $finalNumberOfDownloads_Classic, $logDate);
}
else // file exists
{
	// Retrieve stats
	$arrayStatsLogs		= json_decode(file_get_contents($tmp_pathFileName), true);
	$lastDayUpdateStats 	= $arrayStatsLogs['logs']['lastDayUpdateStats'];
	$firstDayOfStats 	= $arrayStatsLogs['logs']['firstDayOfStats'];

	if ($lastDayUpdateStats != $logDate) // First update of the day
	{
		$downloadsAll = $arrayStatsLogs['logs']['downloads_all'] + $finalNumberOfDownloads_Classic + $finalNumberOfDownloads_Ecole ;
		$stats = createStats($stats, $finalNumberOfDownloads_Ecole, $finalNumberOfDownloads_Classic, $downloadsAll, $logDate, $firstDayOfStats);
	}
	else // Script already launched today : we don't count the same download twice
	{
		$downloadsAll = $arrayStatsLogs['logs']['downloads_all'] - $arrayStatsLogs['logs']['downloads_today_ecole'] - $arrayStatsLogs['logs']['downloads_today_classic'] ;
		$downloadsAll += $finalNumberOfDownloads_Classic + $finalNumberOfDownloads_Ecole ;
		$stats = createStats($stats, $finalNumberOfDownloads_Ecole, $finalNumberOfDownloads_Classic, $downloadsAll, $logDate, $firstDayOfStats);
	}
}
?>
