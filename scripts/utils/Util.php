<?php

class Util
{
	public $debug = "error";

	/*
	 * @debugMode = all || error
	 * */
	public function setDebug($debugMode)
	{
		$this->debug = $debugMode;
	}

	/**
	 * colorize $this->output
	 */
	public function out($text, $color = null, $exit = false, $newLine = true)
	{
		$styles = array(
		'success' => "\033[0;32m%s\033[0m",
		'error' => "\033[31;31m%s\033[0m",
		//'param' => "\033[31;31m%s\033[0m",
		'info' => "\033[33;33m%s\033[0m",
		);

		$format = '%s';

		if (isset($styles[$color])) {
		$format = $styles[$color];
		}

		if ($newLine) {
		$format .= PHP_EOL;
		}

		if ($this->debug == "all") {
			printf($format, $text);
		} elseif($this->debug == "error" && ($color == "error" || $color == "param")){
			printf($format, $text);
		}

		if ($exit) {
		    exit();
		}
	}

	/**
	 * return plop // plap // ploup OR "Array is empty"
	 */
	public function array_toString ($array)
	{
		$tmp_array = '';
		$flag = false;

		foreach ($array as $_db){
			$flag = true;
			$tmp_array .= $_db . ' // ';
		}
		if ($flag) {
			return substr($tmp_array, 0, -3);
		}
		else return 'Array is empty';
	}

	/**
	 * return aliciaKeys || keyskeysBangBang || otherKey OR "Array is empty"
	 */
	public function getAllKeysFromAnArray ($array)
	{
		$tmp_res = '';
		$flag = false;

		foreach ($array as $key => $value){
			$flag = true;
			$tmp_res .= $key . ' || ';
		}
		if ($flag) {
			return substr($tmp_res, 0, -3);
		}
		else return 'Array is empty';
	}

	/**
	 * return associative array OR -1 OR exit
	 * PHP Warning hidden (with @)
	 */
	public function checkJSONAndReturnArray ($pathJSONFile, $exitIfError = true)
	{
		if (!$tmp_array = json_decode(@file_get_contents($pathJSONFile),true))
		{
			if ($exitIfError) {
				$this->out("### Error in reading " . $pathJSONFile . ". Check JSON format. ", 'error', true);
			} else {
				$this->out("### Error in reading " . $pathJSONFile . ". Check JSON format. ", 'error');
				return -1;
			}
		}
		else
		{
			return $tmp_array;
		}
	}

	/**
	 * - check if the key/value is recorded in the $pathToJSONFile
	 * - if not, ask user to set the value for the @keyToSearch
	 * (- check that the new value is in the array of possibilities)
	 * - write the file with the new data
	 * return OK or the script exits
	 *
	 * + Handle init mode for new service
	 */
	public function checkValueIsRecorded_OrSetIt ($pathToJSONFile, $keyToSearch, $stringToDefineTheValue, $arrayToCheck = null, $_mode = null)
	{
		global $util;

		if (!$tmp_res = json_decode(file_get_contents($pathToJSONFile),true)) {
			$this->out("### Error in reading" . $pathToJSONFile . ". Check JSON format. ", 'error', true); // exit
		}
		else {
			if (array_key_exists($keyToSearch, $tmp_res)) {
				return 'OK';
			}
			else { // Asking user to fill the array for the new service

				// Init mode ? Could be null but not different
				$notInit = ($_mode == "init") ? false : true;
				if ($notInit) {
					$this->out("### The service \"$keyToSearch\" is not recorded yet or we don't have all the informations.", 'error'); 
					$this->out("### Call script as follows : php main.php $keyToSearch init", 'error', true); // exit
				}
				$testNewValue = false;

				do {
					$this->out("### Please write the value corresponding to : " . $stringToDefineTheValue, 'param') ;
					if ($arrayToCheck != null) {
						$this->out("### Possible choices : " . $util->array_toString($arrayToCheck));
					}
					$handle = fopen ('php://stdin','r');
					$line = fgets($handle);
					$newValue = trim($line);

					// Check the new value or not
					if ($arrayToCheck != null) {
						if (!in_array($newValue, $arrayToCheck)) {
							$flagError = true;
							$message = "\"" . $newValue . "\" is not a good choice. " . PHP_EOL . 
							"### Should be one of the following list : " . $util->array_toString($arrayToCheck);
							$this->out($message, 'error');
						} else {
							$testNewValue = true;
						}
					} else {
						$testNewValue = true;
					}
				} while ($testNewValue == false);

				$tmp_res[$keyToSearch] = $newValue;
				if(file_put_contents($pathToJSONFile, json_encode($tmp_res)) != FALSE) {
					$this->out("### Success in writing the new value '$newValue'", "success") ;
					$this->out("### If you want to add a value or just change, go to file here : " . $pathToJSONFile, 'info') ;
					return 	'OK';
				}
				else {
					$this->out("### Problem to write the new value", 'error', true); // exit
				}
			}
		}
	}

	/**
	 * - create JSON $pathFile, if not created, with $infosFile
	 * - check if variables are well recorded (thanks to $defaultInfo)
	 * - if not, ask user to set the value
	 * return : an associative array with the final infos
	 */
	public function checkVariablesOrSetThem ($pathFile, $infosFile, $defaultInfo)
	{
		// Deals with the creation of the infosFile
		if(!$this->createFileAndHisContent ($pathFile, $infosFile)) {
			$this->out("### Problem in writing infosFile", 'error', true);
		}

		// Check if all informations are filled in pathFile
		if (!$tmpApiInfos = json_decode(file_get_contents($pathFile),true)) {
			$this->out("### Error in reading " . $pathFile . ". Check JSON format. " , 'error', true);
		}

		$modified = "false";
		// Asking user to fill missing informations
		foreach ($tmpApiInfos as $info_key => $info_value) {
			if ($info_value == $defaultInfo) {
				$handle = fopen ('php://stdin','r');
				$this->out("### Set the value for " . $info_key . " : ",'param');
				$line = fgets($handle);
				$tmpApiInfos[$info_key] = trim($line);
				// $this->out("### You set : " . $info_key . " with " . $tmpApiInfos[$info_key], 'success');
				$modified = "true";
			}
		}
		if ($modified == "true") {
			// Write file with the recent values
			if (file_put_contents($pathFile, json_encode($tmpApiInfos))) {
				$this->out("### You can change the infosFile here : " . $pathFile, 'info');
			}
			else {
				$this->out("### Error in writing infosFile", 'error');
			}
		}

		// Retrieve infosFile or exit
		if (!$finalInfos = json_decode(file_get_contents($pathFile),true)) {
			$this->out("### Error in reading infosFile in " . $pathFile . " .Check JSON format. ", 'error', true);
		}

		return $finalInfos;
	}

	/**
	 * - check if the file already exists
	 * - if not, create the file and put an array in it (transform it in JSON)
	 * return true if file already exists or was well created | false otherwise
	 */
	public function createFileAndHisContent ($pathFile, $arrayContent)
	{
		$flagCreateFile = true;
		if (file_exists($pathFile) && is_dir($pathFile))
		{
			$this->out("### A folder was named like our dbInfos", 'info');
			rename($pathFile, $pathFile . "_DIR_RENAMED");
			$flagCreateFile = file_put_contents($pathFile, json_encode($arrayContent));
		}
		elseif (!file_exists($pathFile))
		{
			$flagCreateFile = file_put_contents($pathFile, json_encode($arrayContent));
		} else
		{
			$this->out("### $pathFile already exists", "info");
		}

		return $flagCreateFile;
	}

	/**
	 * get value with a key of an associative array NO nested array
	 * @pathJSONFile : path to the JSON file
	 * @key 	 : the key
	 * @exit	 : boolean to accept the script to exit if the file doesn't exists
	 * return	 : the value || array of values
	 */
	public function getValueWithKey_inJSONFile ($pathJSONFile, $key, $exit = true)
	{
		if (!$tmp_array = json_decode(file_get_contents($pathJSONFile),true)) {
			if ($exit) {
				$this->out("### Error in reading " . $pathJSONFile . ". Check JSON format. ", 'error', true);
			} else {
				$this->out("### Error in reading " . $pathJSONFile . ". Check JSON format. ", 'error');
				return -1;
			}
		} else {
			if (array_key_exists($key, $tmp_array))	{
				return $tmp_array[$key];
			}
			else $this->out("### '$key' doesn't exist. Should not happened ", 'error', true);
		}
	}

	/**
	 * get value with a key of an associative array
	 * handle Nested array
	 * @pathJSONFile : path to the JSON file
	 * @key 	 : the key
	 * @exit	 : boolean to accept the script to exit if the file doesn't exists
	 * return	 : array of values (could have only one)
	 */
	public function getValuesWithKey_inJSONFile ($pathJSONFile, $key, $exit = true) {

		if (!$arrayToScan = json_decode(file_get_contents($pathJSONFile),true)) {
			if ($exit) {
				$this->out("### Error in reading " . $pathJSONFile . ". Check JSON format. ", 'error', true);
			} else {
				$this->out("### Error in reading " . $pathJSONFile . ". Check JSON format. ", 'error');
				return -1;
			}
		} else {
			$arrayValues = array();

			foreach ($arrayToScan as $innerKey => $innerValue) {

				if ($innerKey == $key ) {
					// Multiple values
					if (is_array($innerValue)){
						foreach($innerValue as $innerInnerKey => $innerInnervalue){
							array_push($arrayValues, $innerInnervalue);
						}
					} else {
					// One value
						array_push($arrayValues, $innerValue);
						return $arrayValues;
					}
				}
			}
			return $arrayValues;
		}
	}

	public function checkValuesInArray ($arrayValues, $arrayValuesAllowed, $pathFileToModifyValue)
	{
		foreach ($arrayValues as $newValue) {
			if (!in_array($newValue, $arrayValuesAllowed))
			{
				$message = "### \"" . $newValue . "\" is not accepted. " . PHP_EOL .
				"### Should be one of the following list : " .  $this->array_toString($arrayValuesAllowed);
				$this->out($message, 'error');
				$this->out("### You should change it here : $pathFileToModifyValue.", 'error', true); // exit
			}
		}
	}

	/*
	 * // NOT USED ANYMORE //
	 * Ask validation to the user when he tries to put a file not in a web folder
	 * @_pathToWeb : path for the file
	 * return yes or no according to his choice
	 * */
	public function confirmationToWriteInThisFolder ($_pathToWeb)
	{

		if (1 != preg_match("/\/web\//",$_pathToWeb) && 1 != preg_match("/\/www\//",$_pathToWeb))
		{
			$handle = fopen ("php://stdin","r");
			$this->out("### Do you really want to put your file in a none-web folder ($_pathToWeb) ? (y/n)", "error");
			$goodResponse = 'nope';
			while ($goodResponse == 'nope')
			{
				$line = fgets($handle);
				$res = trim($line);

				switch ($res) {
					case "y":
						$goodResponse = 'yep';
						return 'yes';
						break;
					case "n":
						$goodResponse = 'yep';
						return 'no';
						break;
					default:
						break;
				}
			}
		}
		else return 'yes';
	}

	/*
	 * Copy an entire directory.
	 * Note to do your own check to make sure the directory exists that you first call it on.
	 * @src : source
	 * @dst : destination
	 * return true if the entire copy was a success, false at the first error of copy
	 * */
	public function recurse_copy($src,$dst) {
		$dir = opendir($src);
		@mkdir($dst); // Remove warning when created folder thanks to @
		while(false !== ( $file = readdir($dir)) ) {
			if (( $file != '.' ) && ( $file != '..' )) {
				if ( is_dir($src . '/' . $file) ) {
					$this->recurse_copy($src . '/' . $file,$dst . '/' . $file);
				}
				else {
					$res = copy($src . '/' . $file,$dst . '/' . $file);
					if (!$res) { return false; }
				}
			}
		}
		closedir($dir);
		return true;
	}
}
?>
