<?php

// ################################################
// ############## THIS IS A PATTERN ###############
// ########## THAT CAN BE USED AND COPY ###########
// ################################################

/*
 * include by dispatcher.php
 *
 * Retrieve $stats
 *
 * */

// ################## INCLUDE #####################
$pathToUtil_pattern = "Util_pattern.php";
include ($pathToUtil_pattern);


// ################# CONSTANTS #################
$pathReqService	= "req_service.php";

// ############## MAIN SCRIPT ###################
$util->out("### Stats with NAME_OF_MODULE", "info");

// ################### STATS #####################

// add values to $stats which is an instance of Stats
switch ($u_service) {
	case "NAME_OF_SERVICE":
		include($pathReqService);
		break;
	default:
		$stats->site = "default site name";
		$util->out("### This service is not initialized for rest_json", "error", true); //exit
		break;
}
?>
