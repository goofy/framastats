<?php

/*
 * include by dispatcher.php
 *
 * Handle with database information, connection and request.
 * Retrieve $stats
 *
 * */

// ################## INCLUDE #####################
$pathToUtil_mySQL = "Util_mySQL.php";
include ($pathToUtil_mySQL);

// ##############################################

$dbInfos = array (
'db_host' 	=> $defaultInfo,
'db_dbname' 	=> $defaultInfo,
'db_usr' 	=> $defaultInfo,
'db_pswrd' 	=> $defaultInfo
);

// ################# CONSTANTS #################

$pathReqFramalibre 	= "req_framalibre.php";
$pathReqFramadate 	= "req_framadate.php";
$pathReqFramabook 	= "req_framabook.php";
$pathReqFramablog 	= "req_framablog.php";

// ############## MAIN SCRIPT ###################
$util->out("### Stats with mySQL", "info");

// Manage database informations
$finalDbInfos = $util->checkVariablesOrSetThem($pathDbInfos, $dbInfos, $defaultInfo);

// Database connection
try
{
	$dBase = new PDO('mysql:host=' . $finalDbInfos['db_host'] . ';dbname='. $finalDbInfos['db_dbname'] . ';charset=utf8', $finalDbInfos['db_usr'], $finalDbInfos['db_pswrd']);
	$util->out("### Established connection in the database : " . $finalDbInfos['db_dbname'], "success");
}
catch(Exception $e)
{
	$util->out("### Error in database connection : ".$e->getMessage(), "error");
	$util->out("### Check infos in this file : ". $pathDbInfos, "error", true);
}

// ################### STATS #####################

// add values to $stats which is an instance of Stats
switch ($u_service) {
	case "framalibre":
		include($pathReqFramalibre);
		break;
	case "framadate":
		include($pathReqFramadate);
		break;
	case "framabook":
		include($pathReqFramabook);
		break;
	case "framablog":
		include($pathReqFramablog);
		break;
	default:
		$util->out("### This service is not initialized for mySQL", "error", true); //exit
		break;
}
?>
